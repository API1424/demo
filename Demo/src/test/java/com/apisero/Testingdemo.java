package com.apisero;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
public class Testingdemo {
	static Prime p;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		p = new Prime();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test1() {
		assertEquals(p.isPrime(11), true);
	}
	
	@Test
	public void test2() {
		assertEquals(p.isPrime(100), false);
	}
	
}
